meta
{
	title: "Address validator (Serbia)";
	description: "This validator is a tool for JOSM that ensures the quality and accuracy of address points in Serbia before uploading them to OpenStreetMap. It verifies that each address point has the required tags and follows the logical rules for Serbian addresses.";
	author: "borovac";
	version: "1.0.1_2024-05-20";
}

/*  Address warning/error validator (applies to Serbia)*/


way[highway][name:sr_Latn][inside("RS")] {
  throwError: tr("Correct name:sr_Latn to name:sr-Latn.", "{0.tag}");
	}
	
way[highway][short_name:sr_Latn][inside("RS")] {
  throwError: tr("Correct short_name:sr_Latn to short_name:sr-Latn.", "{0.tag}");
	}

way[highway][loc_name:sr_Latn][inside("RS")] {
  throwError: tr("Correct loc_name:sr_Latn to loc_name:sr-Latn.", "{0.tag}");
	}
	
way[highway][old_name:sr_Latn][inside("RS")] {
  throwError: tr("Correct old_name:sr_Latn to old_name:sr-Latn.", "{0.tag}");
	}
	
way[highway][alt_name:sr_Latn][inside("RS")] {
  throwError: tr("Correct alt_name:sr_Latn to alt_name:sr-Latn.", "{0.tag}");
	}

way[highway][noname][name][inside("RS")] {
  throwWarning: tr("Check the noname tag.", "{0.tag}");
	}

way[highway][fixme*="name"][name][inside("RS")] {
  throwWarning: tr("Check the fixme tag.", "{0.tag}");
	}

way[highway][note*="name"][name][inside("RS")] {
  throwWarning: tr("Check the note tag.", "{0.tag}");
	}

way[highway][name][!name:sr][!name:sr-Latn][!ref:RS:ulica][inside("RS")] {
  throwWarning: tr("Missing name:sr and name:sr-Latn and ref:RS:ulica tags.", "{0.tag}");
	}

way[highway][name][!name:sr][name:sr-Latn][!ref:RS:ulica][inside("RS")] {
  throwWarning: tr("Missing name:sr and ref:RS:ulica tags.", "{0.tag}");
	}

way[highway][name][name:sr][!name:sr-Latn][!ref:RS:ulica][inside("RS")] {
  throwWarning: tr("Missing name:sr-Latn and ref:RS:ulica tags.", "{0.tag}");
	}

way[highway][name][name:sr][name:sr-Latn][!ref:RS:ulica][inside("RS")] {
  throwWarning: tr("Missing ref:RS:ulica tag.", "{0.tag}");
	}

way[highway][name][!name:sr][name:sr-Latn][ref:RS:ulica][inside("RS")] {
  throwWarning: tr("Missing name:sr tag.", "{0.tag}");
	}

way[highway][name][name:sr][!name:sr-Latn][ref:RS:ulica][inside("RS")] {
  throwWarning: tr("Missing name:sr-Latn tag.", "{0.tag}");
	}

way[highway][!name][!name:sr][name:sr-Latn][!ref:RS:ulica][inside("RS")] {
  throwWarning: tr("Missing name, name:sr and ref:RS:ulica tags.", "{0.tag}");
	}

way[highway][!name][name:sr][!name:sr-Latn][!ref:RS:ulica][inside("RS")] {
  throwWarning: tr("Missing name:sr-Latn and ref:RS:ulica tags.", "{0.tag}");
	}

way[highway][!name][!name:sr][name:sr-Latn][ref:RS:ulica][inside("RS")] {
  throwWarning: tr("Missing name and name:sr tags.", "{0.tag}");
	}
	
way[highway][name][!name:sr][!name:sr-Latn][ref:RS:ulica][inside("RS")] {
  throwWarning: tr("Missing name:sr and name:sr-Latn tags.", "{0.tag}");
	}

way[highway][!name][name:sr][name:sr-Latn][ref:RS:ulica][inside("RS")] {
  throwWarning: tr("Missing name tag.", "{0.tag}");
	}
	
way[highway][name][name:left][name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("In this case name, name:sr and name:sr-Latn tags is unnecessary tag.", "{0.tag}");
	}

way[highway][name:sr][name:left][name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("In this case name, name:sr and name:sr-Latn tags is unnecessary tag.", "{0.tag}");
	}
	
way[highway][name:sr-Latn][name:left][name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("In this case name, name:sr and name:sr-Latn tags is unnecessary tag.", "{0.tag}");
	}
	
way[highway][!name:left][name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left tag.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left and name:left:sr tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][!name:left:sr-Latn][ref:RS:ulica:left][name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr and name:left:sr-Latn tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][!name:left:sr-Latn][!ref:RS:ulica:left][name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr, name:left:sr-Latn and ref:RS:ulica:left tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][!name:left:sr-Latn][!ref:RS:ulica:left][!name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr, name:left:sr-Latn, ref:RS:ulica:left and name:right tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][!name:left:sr-Latn][!ref:RS:ulica:left][!name:right][!name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr, name:left:sr-Latn, ref:RS:ulica:left, name:right and name:right:sr tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][!name:left:sr-Latn][!ref:RS:ulica:left][!name:right][!name:right:sr][!name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr, name:left:sr-Latn, ref:RS:ulica:left, name:right, name:right:sr and name:right:sr-Latn tags.", "{0.tag}");
	}
	
way[highway][name:left][!name:left:sr][!name:left:sr-Latn][!ref:RS:ulica:left][!name:right][!name:right:sr][!name:right:sr-Latn][!ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left:sr, name:left:sr-Latn, ref:RS:ulica:left, name:right, name:right:sr, name:right:sr-Latn and ref:RS:ulica:right  tags.", "{0.tag}");
	}
	
way[highway][name:left][!name:left:sr][!name:left:sr-Latn][!ref:RS:ulica:left][name:right][!name:right:sr][!name:right:sr-Latn][!ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left:sr, name:left:sr-Latn, ref:RS:ulica:left, name:right:sr, name:right:sr-Latn and ref:RS:ulica:right  tags.", "{0.tag}");
	}

way[highway][name:left][name:left:sr][!name:left:sr-Latn][!ref:RS:ulica:left][name:right][!name:right:sr][!name:right:sr-Latn][!ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left:sr-Latn, ref:RS:ulica:left, name:right:sr, name:right:sr-Latn and ref:RS:ulica:right  tags.", "{0.tag}");
	}

way[highway][name:left][name:left:sr][!name:left:sr-Latn][!ref:RS:ulica:left][name:right][name:right:sr][!name:right:sr-Latn][!ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left:sr-Latn, ref:RS:ulica:left, name:right:sr-Latn and ref:RS:ulica:right  tags.", "{0.tag}");
	}
	
way[highway][name:left][name:left:sr][name:left:sr-Latn][!ref:RS:ulica:left][name:right][name:right:sr][!name:right:sr-Latn][!ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing ref:RS:ulica:left, name:right:sr-Latn and ref:RS:ulica:right  tags.", "{0.tag}");
	}
	
way[highway][name:left][name:left:sr][name:left:sr-Latn][!ref:RS:ulica:left][name:right][name:right:sr][name:right:sr-Latn][!ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing ref:RS:ulica:left and ref:RS:ulica:right  tags.", "{0.tag}");
	}

way[highway][name:left][!name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left:sr tag.", "{0.tag}");
	}
	
way[highway][name:left][name:left:sr][!name:left:sr-Latn][ref:RS:ulica:left][name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left:sr-Latn tag.", "{0.tag}");
	}
	
way[highway][name:left][name:left:sr][name:left:sr-Latn][!ref:RS:ulica:left][name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing ref:RS:ulica:left tag.", "{0.tag}");
	}
	
way[highway][name:left][name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][name:right][name:right:sr][name:right:sr-Latn][!ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing ref:RS:ulica:right tag.", "{0.tag}");
	}
	
way[highway][name:left][name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][name:right][name:right:sr][!name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:right:sr-Latn tag.", "{0.tag}");
	}
	
way[highway][name:left][name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][!name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:right tag.", "{0.tag}");
	}
	
way[highway][!name:left][name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][!name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left and name:right tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][!name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr and name:right tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][!name:left:sr-Latn][ref:RS:ulica:left][!name:right][!name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr, name:left:sr-Latn, name:right and name:right tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][!name:left:sr-Latn][ref:RS:ulica:left][!name:right][!name:right:sr][!name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr, name:left:sr-Latn, name:right, name:right and name:right:sr-Latn tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][!name:left:sr-Latn][ref:RS:ulica:left][!name:right][!name:right:sr][!name:right:sr-Latn][!ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr, name:left:sr-Latn, name:right, name:right, name:right:sr-Latn and ref:RS:ulica:right tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][!name:right][!name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr, name:right and name:right tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][!name:left:sr-Latn][ref:RS:ulica:left][!name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr, name:left:sr-Latn and name:right tags.", "{0.tag}");
	}
	
way[highway][name:left][name:left:sr][!name:left:sr-Latn][ref:RS:ulica:left][!name:right][name:right:sr][!name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left:sr-Latn, name:right and name:right:sr-Latn tags.", "{0.tag}");
	}

way[highway][name:left][name:left:sr][!name:left:sr-Latn][ref:RS:ulica:left][!name:right][name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left:sr-Latn and name:right tags.", "{0.tag}");
	}

way[highway][!name:left][name:left:sr][!name:left:sr-Latn][ref:RS:ulica:left][!name:right][name:right:sr][!name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr-Latn, name:right, name:right and name:right:sr-Latn tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][!name:left:sr-Latn][ref:RS:ulica:left][!name:right][name:right:sr][!name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr, name:left:sr-Latn, name:right, name:right and name:right:sr-Latn tags.", "{0.tag}");
	}
	
way[highway][name:left][name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][name:right][!name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:right:sr tag.", "{0.tag}");
	}
	
way[highway][!name:left][name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][name:right][!name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left and name:right:sr tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][name:right][!name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr and name:right:sr tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][!name:left:sr-Latn][ref:RS:ulica:left][name:right][!name:right:sr][name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr, name:left:sr-Latn and name:right:sr tags.", "{0.tag}");
	}
	
way[highway][!name:left][name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][name:right][name:right:sr][!name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left and name:left:sr-Latn tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][name:left:sr-Latn][ref:RS:ulica:left][name:right][name:right:sr][!name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr and name:left:sr-Latn tags.", "{0.tag}");
	}
	
way[highway][!name:left][!name:left:sr][!name:left:sr-Latn][ref:RS:ulica:left][name:right][name:right:sr][!name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr, name:left:sr-Latn and name:left:sr-Latn tags.", "{0.tag}");
	}	
	
way[highway][!name:left][!name:left:sr][!name:left:sr-Latn][!ref:RS:ulica:left][!name:right][name:right:sr][!name:right:sr-Latn][ref:RS:ulica:right][inside("RS")] {
  throwWarning: tr("Missing name:left, name:left:sr, name:left:sr-Latn, ref:RS:ulica:left and name:left:sr-Latn tags.", "{0.tag}");
	}		
	

way[highway]["name:left" =~ /^[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")] {
  throwWarning: tr("Latin character in name:left tag.");
	}
	
way[highway]["name:right" =~ /^[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")] {
  throwWarning: tr("Latin character in name:right tag.");
	}
	
way[highway]["name:left:sr" =~ /^[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")] {
  throwWarning: tr("Latin character in name:left:sr tag.");
	}
	
way[highway]["name:right:sr" =~ /^[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")] {
  throwWarning: tr("Latin character in name:right:sr tag.");
	}
	
way[highway]["name:right:sr-Latn" =~ /^[Ђ-џ]+/][inside("RS")] {
  throwWarning: tr("Cyrillic character in name:right:sr-Latn tag.");
	}
	
way[highway]["name:left:sr-Latn" =~ /^[Ђ-џ]+/][inside("RS")] {
  throwWarning: tr("Cyrillic character in name:left:sr-Latn tag.");
	} 

way[highway]["name" =~ /^[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")] {
  throwWarning: tr("Latin character in name tag.");
	}
  
way[highway]["name:sr" =~ /^[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")] {
  throwWarning: tr("Latin character in name:sr tag.");
	}
	
way[highway]["name:sr-Latn" =~ /^[Ђ-џ]+/][inside("RS")] {
  throwWarning: tr("Cyrillic character in name:sr-Latn tag.");
	} 
	
way[highway]["int_name" =~ /^[Ђ-џ]+/][inside("RS")] {
  throwWarning: tr("Cyrillic character in int_name tag.");
	} 
	
way[highway]["short_name:sr" =~ /^[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")] {
  throwWarning: tr("Latin character in short_name:sr tag.");
	}
	
way[highway]["short_name:sr" =~ /^[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")] {
  throwWarning: tr("Latin character in short_name tag.");
	}
	
way[highway]["loc_name:sr" =~ /^[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")] {
  throwWarning: tr("Latin character in loc_name:sr tag.");
	}
	
way[highway]["loc_name" =~ /^[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")] {
  throwWarning: tr("Latin character in loc_name tag.");
	}
	
way[highway]["old_name:sr" =~ /^[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")] {
  throwWarning: tr("Latin character in old_name:sr tag.");
	}
	
way[highway]["old_name" =~ /^[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")] {
  throwWarning: tr("Latin character in old_name tag.");
	}

way[highway]["alt_name:sr" =~ /^[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")] {
  throwWarning: tr("Latin character in alt_name:sr tag.");
	}
	
way[highway]["alt_name" =~ /^[A-ǌ]+/][inside("RS")] {
  throwWarning: tr("Latin character in alt_name tag.");
	}
	
way[highway]["short_name:sr-Latn" =~ /^[Ђ-џ]+/][inside("RS")] {
  throwWarning: tr("Cyrillic character in short_name:sr-Latn tag.");
	}
	
way[highway]["loc_name:sr-Latn" =~ /^[Ђ-џ]+/][inside("RS")] {
  throwWarning: tr("Cyrillic character in loc_name:sr-Latn tag.");
	}
	
way[highway]["old_name:sr-Latn" =~ /^[Ђ-џ]+/][inside("RS")] {
  throwWarning: tr("Cyrillic character in old_name:sr-Latn tag.");
	}

way[highway]["alt_name:sr-Latn" =~ /^[Ђ-џ]+/][inside("RS")] {
  throwWarning: tr("Cyrillic character in alt_name:sr-Latn tag.");
	}


way[!addr:housenumber][addr:street][building][ref:RS:kucni_broj][inside("RS")] {
  throwWarning: tr("Missing addr:housenumber tag.", "{0.tag}");
	}

way[addr:housenumber][addr:street][building][!ref:RS:kucni_broj][inside("RS")] {
  throwWarning: tr("Missing ref:RS:kucni_broj tag.", "{0.tag}");
	}
  
way[!addr:housenumber][!addr:street][building][ref:RS:kucni_broj][inside("RS")] {
  throwWarning: tr("Missing addr:housenumber and addr:street tags.", "{0.tag}");
	}

way[!addr:housenumber][addr:street][building][!ref:RS:kucni_broj][inside("RS")] {
  throwWarning: tr("Missing addr:housenumber and ref:RS:kucni_broj tags.", "{0.tag}");
	}
  
way[!addr:housenumber][!addr:street][building][ref:RS:kucni_broj][inside("RS")] {
  throwWarning: tr("Missing addr:housenumber and ref:RS:kucni_broj tags.", "{0.tag}");
	}
  
way[addr:housenumber][!addr:street][building][!ref:RS:kucni_broj][inside("RS")] {
  throwWarning: tr("Missing addr:street and ref:RS:kucni_broj tags.", "{0.tag}");
	}
  
node[!addr:housenumber][addr:street][ref:RS:kucni_broj][inside("RS")] {
  throwWarning: tr("Missing addr:housenumber tag.", "{0.tag}");
	}

node[addr:housenumber][addr:street][!ref:RS:kucni_broj][inside("RS")] {
  throwWarning: tr("Missing ref:RS:kucni_broj tag.", "{0.tag}");
	}
  
node[!addr:housenumber][!addr:street][ref:RS:kucni_broj][inside("RS")] {
  throwWarning: tr("Missing addr:housenumber and addr:street tags.", "{0.tag}");
	}

node[!addr:housenumber][addr:street][!ref:RS:kucni_broj][inside("RS")] {
  throwWarning: tr("Missing addr:housenumber and ref:RS:kucni_broj tags.", "{0.tag}");
	}
  
node[!addr:housenumber][!addr:street][ref:RS:kucni_broj][inside("RS")] {
  throwWarning: tr("Missing addr:housenumber and ref:RS:kucni_broj tags.", "{0.tag}");
	}
  
node[addr:housenumber][!addr:street][!ref:RS:kucni_broj][inside("RS")] {
  throwWarning: tr("Missing addr:street and ref:RS:kucni_broj tags.", "{0.tag}");
	}  
  
*[addr:housenumber]["addr:housenumber" =~ /[Ђ-џ]+/][inside("RS")]  {
  throwWarning: tr("Housenumber should be written in Latin.");
	}

*[addr:housenumber]["addr:housenumber" =~ /[A-ZŽŠĐČĆ]+/][inside("RS")] {
  throwWarning: tr("Housenumber is written in capital case and it should be in lower case.");
	}
	
*[addr:housenumber][addr:housenumber =~ /\s/][inside("RS")] {
   throwWarning: tr("Housenumber contains a space character.");
	}
	
	
*[ref:RS:kucni-broj][inside("RS")] {
  throwError: tr("Correct ref:RS:kucni-broj to ref:RS:kucni_broj.", "{0.tag}");
	}

*[addr:street]["addr:street" =~ /[A-ZŽŠĐČĆa-zšđćž]+/][inside("RS")]  {
  throwWarning: tr("Latin character in addr:street tag.");
	}	
	
*[addr:housenumber]["addr:housenumber" =~ /(?<!\w)(?:bb|b b|BB|B B|бб|б б|ББ|Б Б)(?!\w)+/][inside("RS")] {
  throwWarning: tr("Use nohousenumber=yes instead of addr:housenumber=bb.");
	}

*["ref:RS:ulica"]["ref:RS:ulica" !~ /^.{12}$|^.{12};.{12}$+/][inside("RS")]  {
  throwError: tr("Suspicious character count for the "ref:RS:ulica" tag.");
	}	
